package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods
{

	public FindLeadsPage() 
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]") WebElement enterFN;
	@FindBy(how=How.XPATH,using="(//input[@name='lastName'])[3]") WebElement enterLN;
	@FindBy(how=How.XPATH,using="(//input[@name='companyName'])[2]") WebElement enterCN;
	@FindBy(how=How.LINK_TEXT,using="Find Leads") WebElement clickFLead;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[4]") WebElement selectrecord;
	
	
	
	public FindLeadsPage enterFirstName(String data)
	{
		type(enterFN, data);
		return this;
		
	}
	
	public FindLeadsPage enterLastName(String data)
	{
		type(enterLN, data);
		return this;
	}
	
	public FindLeadsPage enterCompanyName(String data)
	{
		type(enterCN, data);
		return this;
	}
	
	public FindLeadsPage ClickFindLead()
	{
		click(clickFLead);
		return this;
	}
	
	public ViewLeadPage selectRecord()
	{
		click(selectrecord);
		return new ViewLeadPage();
	}
}







