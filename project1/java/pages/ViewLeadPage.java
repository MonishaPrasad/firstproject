package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods
{

	public ViewLeadPage() 
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement clickFL;
	@FindBy(how=How.XPATH, using="(//a[@class='subMenuButton'])[3]") WebElement clickedit;
	
	public ViewLeadPage  clickFindLeads() 
	{
	    click(clickFL);
		return this;
	}
	
	public EditLeadPage  clickEdit() 
	{
	    click(clickedit);
		return new EditLeadPage();
	}
	
	
}






