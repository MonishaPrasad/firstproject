package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods
{

	public CreateLeadPage() 
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement typeCN;
	@FindBy(how=How.ID,using="createLeadForm_firstName")WebElement typeFN;
	@FindBy(how=How.ID,using="createLeadForm_lastName")WebElement typeLN;
	@FindBy(how=How.NAME,using="submitButton")WebElement clickCL;
	
	public CreateLeadPage  typeCompanyName(String data) 
	{
	    type(typeCN,data);
		return this;
	}
	
	public CreateLeadPage  typeFirstName(String data) 
	{
	    type(typeFN,data);
		return this;
	}
	
	public CreateLeadPage  typeLastName(String data) 
	{
	    type(typeLN,data);
		return this;
	}
	
	public CreateLeadPage  clickCreateLead() 
	{
	    click(clickCL);
		return this;
	}
	
}







