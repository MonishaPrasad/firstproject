package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods
{

	public EditLeadPage() 
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.NAME, using="firstNameLocal") WebElement typeFNL;
	@FindBy(how=How.NAME, using="lastNameLocal") WebElement typeLNL;
	@FindBy(how=How.XPATH, using="(//input[@class='smallSubmit'])[1]") WebElement clickUpdate;
	
	public EditLeadPage  typeFirstNameLocal(String data) 
	{
	    type(typeFNL,data);
		return this;
	}
	
	public EditLeadPage  typeLastNameLocal(String data) 
	{
	    type(typeLNL,data);
		return this;
	}
	
	public ViewLeadPage clickUpdateButton() 
	{
	    click(clickUpdate);
		return new ViewLeadPage();
	}

	
}







