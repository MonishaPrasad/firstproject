package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() 
	{
		testCaseName = "TC002_CreateLead";
		testDescription ="Creating Lead";
		testNodes = "Leads";
		authors ="Gayatri";
		category = "smoke";
		dataSheetName="TC001";
		sheetName="CreateLead";
	}
	@Test(dataProvider="fetchData")
	public void CreateLead(String uname, String pwd,String CName, String FName, String LName) 
	{
		new LoginPage() .enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.typeCompanyName(CName)
		.typeFirstName(FName)
		.typeLastName(LName)
		.clickCreateLead();
		System.out.println("Created Lead Sucessfully");
		
		
	}

}











