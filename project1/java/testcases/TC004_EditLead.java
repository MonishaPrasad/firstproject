package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods{

	@BeforeTest
	public void setData() 
	{
		testCaseName = "TC004_EditLead";
		testDescription ="Creating Lead";
		testNodes = "Leads";
		authors ="Gayatri";
		category = "smoke";
		dataSheetName="TC001";
		sheetName="EditLead";
	}
	@Test(dataProvider="fetchData")
	public void FindLead(String uname, String pwd,String firstName, String lastName, String companyName,String firstNameLocal, String lastNameLocal)
	{
		new LoginPage() .enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterCompanyName(companyName)
		.ClickFindLead()
		.selectRecord().clickEdit() 
		. typeFirstNameLocal(firstNameLocal)
		.typeLastNameLocal(lastNameLocal)
		.clickUpdateButton();
		System.out.println("Edited Sucessfully");
	}

}











