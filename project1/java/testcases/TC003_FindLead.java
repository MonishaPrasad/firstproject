package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_FindLead extends ProjectMethods{

	@BeforeTest
	public void setData() 
	{
		testCaseName = "TC003_FindLead";
		testDescription ="Creating Lead";
		testNodes = "Leads";
		authors ="Gayatri";
		category = "smoke";
		dataSheetName="TC001";
		sheetName="FindLead";
	}
	@Test(dataProvider="fetchData")
	public void FindLead(String uname, String pwd,String firstName, String lastName, String companyName)
	{
		new LoginPage() .enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterCompanyName(companyName)
		.ClickFindLead()
		.selectRecord();
		System.out.println("FindLead Executed and displayed View Lead Page sucessfully");
	}

}











